﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.U2D;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class AdventureGame : MonoBehaviour
{
   [SerializeField] Text textComponent;
   [SerializeField] State startingState;
    [SerializeField] Image imageComponent;
   

   
    State state;
    // Start is called before the first frame update
    void Start()
    {
        state = startingState;
        textComponent.text = state.GetStateStory();
        imageComponent.sprite = state.GetImageStory();
    }

    // Update is called once per frame
    void Update()
    {
        ManageState();
    }

	private void ManageState()
	{
        var nextStates = state.GetNextStates();
		for (int i = 0; i < nextStates.Length; i++)
		{
            if (Input.GetKeyDown(KeyCode.Alpha1 + i))
         {
                  state = nextStates[i];
            }
        }
        
        textComponent.text = state.GetStateStory();
        imageComponent.sprite = state.GetImageStory();

    }
    
}
